package main

import (
	"fmt"
)

// channel 与 range关键字的配合使用
func main() {
	c := make(chan int) // 创建一个不带缓冲的channel
	go func() {
		for i := 0; i < 5; i++ {
			c <- i
		}
		close(c)
	}()

	//for true {
	//    if data, ok := <- c; ok {
	//        fmt.Println(data)
	//    } else {
	//        fmt.Println("channel可以已经被关闭")
	//        break
	//    }
	//}

	// range会尝试从管道c中获取数据，如果获取不到，则阻塞等待
	// 我们一般可以用range来迭代不断消费channel的操作
	for data := range c {
		fmt.Println(data) // 效果与上面的for true循环一模一样，所以使用range关键字可以简化channel的死循环消费代码
	}

	fmt.Println("Finished...")
}
