package main

import "fmt"

// 万能类型 interface{},类似于java中的Object，这样就可以传入任意类型
// 整个 Golang 中所有的类型，都会实现于 interface{} 这个空接口
func myFunc(arg interface{})  {
    fmt.Println("myFunc is called...")
    //fmt.Println(arg)

    // Golang中为 interface{} 提供了一种断言机制
    book, flag := arg.(Book)
    if flag {
        fmt.Println("arg 是Book类型")
        fmt.Println(book)
    } else {
        fmt.Println("arg 不是Book类型")
        fmt.Println(book)
    }
}

type Book struct {
    id int
    name string
    auth string
}

func main() {
    book1 := Book{1, "三体", "刘慈欣"}
    myFunc(book1)
    myFunc(100)
    myFunc("哈哈哈")
}
