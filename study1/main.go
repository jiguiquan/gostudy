package main  // 入口程序，必须为main

import (
    "fmt"
    "time"
)

// go build 打包windows下的可执行文件
func main() {
    // golang中的表达式，可以加“;”，也可以不加，建议统一不加！
    fmt.Print("先休息一秒钟：")
    time.Sleep(1 * time.Second)
    fmt.Println("hello world!")
}
