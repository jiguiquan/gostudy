package main

import "fmt"

func foo1()  {
    fmt.Println("这是foo1的defer")
    fmt.Println("foo1普通方法体")
}

func foo2()  {
    fmt.Println("这是foo2的defer")
    fmt.Println("foo2普通方法体")
}

/** 很明显，在每一个函数体内部，都是先完成普通方法逻辑，再至下而上地执行defer方法

    普通方法体

    这是foo2的defer
    foo2普通方法体

    这是foo1的defer
    foo1普通方法体

    defer善后工作2
    defer善后工作1
 */
func main() {
    // defer：善后工作，类似于“析构函数”，在函数调用的最后执行
    // 由于是栈结构，所以defer1先入栈，就会后出栈
    defer fmt.Println("defer善后工作1")  // 后
    defer fmt.Println("defer善后工作2")  // 先

    defer foo1()
    defer foo2()

    fmt.Println("main普通方法体")
}
