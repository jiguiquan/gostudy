package main

import "fmt"

// 为什么get和set方法都是指针传递？ —— 因为我们除了要读取类的属性值，还希望可以修改类的属性值
// 为什么get和set方法首字母都是大写？ —— 如果首字母小写的话代表private方法，如果首字母大写代表public方法
type Hero struct {
    id int
    name string
    level int
}

// (h *Hero)：这部分的意思是，当前方式是绑定到谁身上：
// this Hero的话，就代表当前对象
// h *Hero，就代表调用者（对象）的指针
func (h *Hero) Level() int {
    return h.level
}

func (h *Hero) SetLevel(level int) {
    h.level = level
}

func (h *Hero) Name() string {
    return h.name
}

func (h *Hero) SetName(name string) {
    h.name = name
}

func (h *Hero) Id() int {
    return h.id
}

func (h *Hero) SetId(id int) {
    h.id = id
}

func main() {
    // 创建一个对象
    hero := Hero{id: 1, name: "zhangsan", level: 10}
    fmt.Printf("id=%d, name=%s, level=%d\t", hero.Id(), hero.Name(), hero.Level())  //id=1, name=zhangsan, level=10

    hero.SetLevel(11)  // 如果SetLevel方法不是接收指针的话，我们是没有办法修改属性值的
    fmt.Printf("id=%d, name=%s, level=%d", hero.Id(), hero.Name(), hero.Level())  //id=1, name=zhangsan, level=10
}