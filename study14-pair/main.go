package main

import (
    "fmt"
    "io"
    "os"
)

// Golang中任一变量由2部分组成
// 变量 = type + value，这即是所谓的 pair 对
//       type = static type（静态类型int string等） + concrete type（具体类型/结构类型）
// 反射技术，就是依赖于上面的结构分析， 通过变量，得到具体的type和其中的value

func main() {
    var a string
    a = "jiguiquan"  // pair<type:string, value:"jiguiquan">

    var allType interface{}
    allType = a      // pair<type:string, value:"jiguiquan">

    str, flag := allType.(string)
    fmt.Printf("isStr:%v, value is %v", flag, str)   // isStr:true, value is jiguiquan，说明pair是传递的

    fmt.Println()

    // pair<type:*os.File, value:"/tmp/test.txt"文件指针>
    file, err := os.OpenFile("/tmp/test.txt", os.O_RDWR, 0)
    if err != nil {
        fmt.Println("open file error:", err)
    }
    fmt.Println(file)  // &{0xc0000da780}
    fmt.Println(file.Fd())  // 612

    var r io.Reader
    r = file

    var w io.Writer
    w = r.(io.Writer)

    w.Write([]byte("Hello This is a Test!!!\n"))  // 此方法，将会修改/tmp/test.txt 文件中的内容

}