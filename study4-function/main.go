package main

import "fmt"

// 方法一：fmt.Sprintf()：可以格式化拼接字符串
func fun1(a string, b int) string {
    return fmt.Sprintf("%s%d", a, b)
}

// 方法二：Go与Java不一样，支持一次返回多个值（匿名返回）
func fun2(a string, b int) (string, int)  {
    return a + a, b + b
}

// 方法三：Go与Java不一样，支持一次返回多个值（指定形参名称返回）
// r1，r2的域空间就知识在 fun3 函数体内部
func fun3(a string, b int) (r1 string, r2 int)  {
    fmt.Println("提前打印r1 =", r1)  // 此时为字符串变量的初始值：空串“”
    fmt.Println("提前打印r2 =", r2)  // 此时为int型变量的初始值：0
    r1 = a + "zidan"
    r2 = b + 100
    return
}

// 方法四：如果多个返回值类型相同，可以合并书写
func fun4(a string, b int) (r1, r2 int)  {
    r1 = b + 100
    r2 = b + 1000
    return
}

func main() {
    c := fun1("aaa", 100)
    fmt.Println("c =", c)
    // 打印结果：c = aaa100

    ret1, ret2 := fun2("jiguiquan", 666)
    fmt.Println("ret1 =", ret1, ", ret2 =", ret2)
    // 打印结果：ret1 = jiguiquanjiguiquan , ret2 = 1332

    re1, re2 := fun3("jiguiquan", 666)
    fmt.Println("re1 =", re1, ", re2 =", re2)
    // 打印结果：re1 = jiguiquanjiguiquan , re2 = 1332
}
