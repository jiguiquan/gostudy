package main

import "fmt"

// 切片Slice的4种声明方式：
func main() {
    // 方法1：直接定义一个空的slice切片，同时给前几个元素赋值
    slice1 := []int{1,2,3}
    fmt.Printf("len = %d, slice1 = %v\n", len(slice1), slice1)  //len = 3, slice = [1 2 3]

    // 方法2：直接定义一个空的slice切片，不进行初始赋值
    var slice2 []int
    fmt.Printf("len = %d, slice2 = %v\n", len(slice2), slice2)  //len = 0, slice = []
    // 此时，如果我们直接对slice2的索引元素金鼎赋值，会直接报错
    //slice2[0] = 1  // index out of range [0]
    slice2 = make([]int, 3)  // 为slice2开辟一块空间，之后就可以给slice2的索引元素赋值了
    slice2[0] = 10
    slice2[1] = 20
    fmt.Printf("len = %d, slice2 = %v\n", len(slice2), slice2)  //len = 3, slice = [10 20 0]

    // 方法3：使用make一步到位，var定义
    var slice3 []int = make([]int, 3)
    slice3[0] = 10
    slice3[1] = 20
    fmt.Printf("len = %d, slice3 = %v\n", len(slice3), slice3)  //len = 3, slice3 = [10 20 0]

    // 方法4：通过make，动态推断
    slice4 := make([]int, 3)
    slice4[0] = 10
    slice4[1] = 20
    fmt.Printf("len = %d, slice4 = %v\n", len(slice4), slice4)  //len = 3, slice4 = [10 20 0]

    // 判断一个slice是否为空
    var slice5 []int
    if slice5 == nil {
        fmt.Println("slice5是个空数组")  // true
    } else {
        fmt.Println("slice5不是空数组")
    }
}
