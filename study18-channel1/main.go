package main

import (
	"fmt"
)

// chan的基本定义和简单使用（类似于阻塞队列）
func main() {
	// 定义一个channel
	c := make(chan int) // 不带数字，代表无缓冲channel

	go func() {
		defer fmt.Println("goroutine运行结束！")
		fmt.Println("goroutine正在运行...")

		// 向管道c中发送一个666
		c <- 666
	}()

	// 从管道c中接收数据，并赋值给num
	num := <-c // 阻塞等待（有点像阻塞队列）
	fmt.Printf("num =%d\n", num)
	fmt.Println("main goroutine结束！")
}
