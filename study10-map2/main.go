package main

import "fmt"

// map的使用：遍历，删除，修改，传递
func main() {
    cityMap := map[string]string{
        "China":"Beijing",
        "Japan":"Tokyo",
        "USA":"NewYork",
    }

    // map的遍历
    for key, value := range cityMap {
        fmt.Printf("%s:%s \t", key, value)  //China:Beijing 	Japan:Tokyo 	USA:NewYork
    }

    fmt.Println()

    // map的删除
    delete(cityMap, "Japan")
    for key, value := range cityMap {
        fmt.Printf("%s:%s \t", key, value)  //China:Beijing 	USA:NewYork
    }

    fmt.Println()

    // map的修改
    cityMap["USA"] = "DC"
    for key, value := range cityMap {
        fmt.Printf("%s:%s \t", key, value)  //China:Beijing 	USA:DC
    }

    fmt.Println()

    // map类型的传参(指针传递)
    printMap(cityMap) // China:Beijing 	USA:DC
}

func printMap(myMap map[string]string)  {
    for key, value := range myMap {
        fmt.Printf("%s:%s \t", key, value)
    }
}
