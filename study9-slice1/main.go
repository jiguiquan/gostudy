package main

import "fmt"

// 动态数组 的用法和 定长数组用法一模一样，只是不要定义长度
func printAlice(alice []int)  {
    for index, value := range alice {
        fmt.Printf("%d - %d \t", index, value)  // 0 - 1 	1 - 2 	2 - 3 	3 - 4
    }

    fmt.Println()

    // 如果只想使用value值，不想使用index，则可以使用匿名参数
    for _, value := range alice {
        fmt.Printf("%d \t", value)  // 1  2 	3  4
    }

    fmt.Println()

    // 最后修改下数组的值看看
    alice[0] += 10
}

func main() {
    myAlice1 := []int{1,2,3,4}
    printAlice(myAlice1)  // 动态数组不同于定长数组，动态数组为“指针传递”，而定长数组为“值传递”

    for _, value := range myAlice1 {
        fmt.Print(value, "\t")  // 11	2	3	4
    }
}