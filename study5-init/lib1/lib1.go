package lib1

import "fmt"

// init 方法会优先于所有方法而执行
func init()  {
    fmt.Println("lib1.init()方法被执行了！")
}

/* 注意区分方法的大小写
    小写：lib1Test：只能在当前包内进行调用
    大写：Lib1Test：对外暴露export，可以被外部调用
 */
func Lib1Test()  {
    fmt.Println("Lib1Test()...")
}