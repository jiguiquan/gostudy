package main

// 暂时我们需要从 $GoPATH/src 开始，写全包的路径
// 以后使用 go mod 模块化管理该项目，就可以不需要了
import (
    "fmt"
    //"study5-init/lib1"
    // 1、别名导包方式：当包名很长时候，我们可以给他们起个别名，之后就用别名调用即可
    //mylib1 "study5-init/lib1"
    // 2、“.”号直接导入lib1：之后调用lib1中的方法，就像调用本地方法一样，不需要通过lib1.Lib1Test()进行调用（不推荐使用，容易多包方法名称冲突）
    . "goStudy/study5-init/lib1"

    // 3、匿名导包方式：不想显式调用lib2中的普通方法，但是又想执行lib2包中的init()方法
    // 如果前面不加“_“下划线的话，导入lib2包后，代码中不使用，则Go的编译器会报错（严谨）
    _"goStudy/study5-init/lib2"
)

func init()  {
    fmt.Println("main.init()...")
}

/** 方法调用顺序
    单个包中：import —> const —> var —> init，结束初始化；
    多个包中：如果一个包导入了其他包，那么先完成被导入包的初始化工作！
 */
func main() {
    Lib1Test()
    //lib1.Lib1Test()
    //mylib1.Lib1Test()
    //lib2.Lib2Test()
}
/** 打印结果
   lib1.init()方法被执行了！
   lib2.init()方法被执行了！
   main.init()...
   Lib1Test()...
   Lib2Test()...
*/
