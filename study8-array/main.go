package main

import "fmt"

/**
    \t：空格
    \n：换行
    \r：制表符Tab

    %T：打印数据类型
    %v：打印任意类型的数据体
 */

// 定长数组传参，类型必须完全一致，包括长度（值传递）
// 否则报错：Cannot use 'myArr1' (type [10]int) as type [5]int
func printArr(arr [5]int)  {
    for index, value := range arr {
        fmt.Printf("%d - %d \t", index, value)
    }
}

func main() {
    // 1、定义一个固定长度的数组（每个值都为int默认值）
    var myArr1 [10]int
    for i := 0; i < len(myArr1); i++ {
        fmt.Print(myArr1[i], "\t") //0	0	0	0	0	0	0	0	0	0
    }

    fmt.Println()  // 强行换行

    // 2、定义一个固定长度的数组（给前几个值赋值）
    myArr2 := [5]int{0, 1, 2}
    for index, value := range myArr2 {
        fmt.Printf("%d - %d \t", index, value)  //0 - 0 	1 - 1 	2 - 2 	3 - 0 	4 - 0
    }

    fmt.Println()  // 强行换行

    fmt.Printf("myArr1的数据类型为：%T\n", myArr1)  // [10]int
    fmt.Printf("myArr2的数据类型为：%T", myArr2)  // [5]int

    fmt.Println()  // 强行换行

    printArr(myArr2)
}
