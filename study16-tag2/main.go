package main

import (
	"encoding/json"
	"fmt"
)

// 结构体标签在结构体中的应用

type Movie struct {
	Title  string   `json:"title"`
	Year   int      `json:"year"`
	Price  float64  `json:"price"`
	Actors []string `json:"actors"`
}

func main() {
	movie := Movie{"喜剧之王", 2000, 10.5, []string{"周星驰", "张柏芝"}}
	jsonstr, error := json.Marshal(movie)
	if error != nil {
		fmt.Println("json marshal failed")
		return
	}
	fmt.Printf("jsonStr = %s\n", jsonstr) // jsonStr = {"title":"喜剧之王","year":2000,"price":10.5,"actors":["周星驰","张柏芝"]}

	// 再将字符串反序列化为Movie对象
	var myMovie Movie
	error = json.Unmarshal(jsonstr, &myMovie) // 需要传入指针（可以理解成：想修改对象值，就得传入指针）
	if error != nil {
		fmt.Println("jsonStr unmarshal failed")
		return
	}
	fmt.Println("myMovie = ", myMovie) // myMovie =  {喜剧之王 2000 10.5 [周星驰 张柏芝]}
}
