package main

import "fmt"

// 接收值类型的变量
func changeValue(p int)  {
    p = 10
}

// *接收指针类型的变量
func changePoint(p *int)  {
    *p = 100
}

func main() {
    var a int = 1

    changeValue(a)  // 直接使用，代表传递的是a的值
    fmt.Println("经过changeValue后a的值为：", a)  // = 1

    changePoint(&a) // &a 代表传递的是a的指针（内存地址） // = 100
    fmt.Println("经过changePoint后a的值为：", a)

    // 一级指针常见，二级指针很少用到
    var p *int = &a
    var pp **int = &p
    fmt.Println("一级指针：", p)  //0xc0000ac058
    fmt.Println("二级指针：", pp)  //0xc0000d8020
}
