package main

import "fmt"

/** channel关闭的特点
    如果不主动去关闭channel的话，会报错：fatal error: all goroutines are asleep - deadlock!
意思是：go编译器检测到，所有的goroutine都已经结束了，不会再有人往channel中写数据了，而主线程却还在尝试获取，那么相当于就死锁了！
    如果我们显式执行close(c)，那么就可以正常结束，不报错了！
*/

/** channel的特点：
  1、channel不像文件一样，需要经常去关闭，只有当我们确定没有任何数据发送者时，或者想显示地结束range循环之类的时候，才主动去关闭channel；
  2、关闭channel后，无法再向channel中发送数据（引发panic报错，导致接受者立即获得到一个0值）；
  3、关闭channel后，可以继续从channel中接收数据；
  4、对于nil channel，无论收发都会被阻塞（nil channel = 直接var c chan，而不进行make操作）；
*/
func main() {
	c := make(chan int) // 创建一个不带缓冲的channel
	go func() {
		for i := 0; i < 5; i++ {
			c <- i
		}
		close(c)
	}()

	for true {
		// ok=true，channel没有关闭
		// ok=false，channel已经关闭
		if data, ok := <-c; ok {
			fmt.Println(data)
		} else {
			fmt.Println("channel可以已经被关闭")
			break
		}
	}

	fmt.Println("Finished...")
}
