package main

import "fmt"

type Human struct {
    name string
    sex string
}

func (this *Human) Eat()  {
    fmt.Println("Human.Eat()...")
}

func (this *Human) Walk()  {
    fmt.Println("Human.Walk()...")
}

// 再定义一个结构体SuperMan，继承于Human结构体
type SuperMan struct {
    Human  // 代表继承于Human类
    weight int
}

func (this *SuperMan) Eat()  {
    this.Human.Eat()
}

func (this *SuperMan) Fly()  {
    fmt.Println("SuperMan.Fly()...")
}

func main() {
    h := Human{"张三", "男"}
    h.Eat()
    h.Walk()
    sm := SuperMan{Human{"李四", "男"}, 100}
    sm.Eat()
    sm.Fly()
    sm.sex = "女"
    fmt.Print(sm)  // {{李四 女} 100}，说明可以直接修改的，如果从外部，就得暴露Set方法了
}
