package main

import "fmt"

// 变量声明var、:=
/*
Printf - 格式化打印：
    %v   自动匹配类型
    %d   十进制整数
    %x   十六进制整数
    %f      浮点数
    %s      字符串
*/

func main() {
    // 方法一：声明一个变量，默认值为0
    var a int
    fmt.Println("a = ", a)  // 结果为0

    // 方法二：声明一个变量，显式赋初始化值
    var b int = 100
    fmt.Println("b = ", b)

    // 方法三：省去变量类型，胜过自动匹配当前变量的数据类型（不推荐）
    var c = 200
    fmt.Println("c = ", c)
    fmt.Printf("type of c = %T\n", c) // 格式化打印
    var cc = "abcd"
    fmt.Printf("cc = %s, type of cc = %T\n", cc, cc) //cc = abcd, type of cc = string

    // 方法四：省去var关键字，直接自动匹配（最常用的变量声明方式）
    // 通过“:=”，自动推断出d的数据类型
    // := 方式，只能用在函数体内部，不能用来声明全局变量
    d := 300
    fmt.Printf("d = %d, type of d = %T\n", d, d)

    // 方法五：可以在一行内，同时声明多个变量
    var x, y int = 100, 200
    fmt.Println("x = ", x, ",y = ", y)
    var xx, yy = 300, "jiguiquan"  // 即使变量类型不相同，依然可以在一行内声明
    fmt.Println("xx = ", xx, ",yy = ", yy)

    // 方法六：多行内完成多变量声明
    var (
        xxx int = 100
        yyy string = "zidan"
    )
    fmt.Println("xxx = ", xxx, ",yyy = ", yyy)
}
