package main

import "fmt"

// 接口：本质也是一个指针
type AnimalIF interface {
    Sleep()     // 会睡觉
    GetColor() string // 获取颜色
    GetType() string  // 获取种类
}

// 具体的类Cat
type Cat struct {
    color string
}

func (this *Cat) Sleep() {
    fmt.Println("cat is sleeping")
}

func (this *Cat) GetColor() string {
    return this.color
}

func (this *Cat) GetType() string {
    return "Cat"
}

// 具体的类Dog
type Dog struct {
    color string
}

func (this *Dog) Sleep() {
    fmt.Println("dog is sleeping")
}

func (this *Dog) GetColor() string {
    return this.color
}

func (this *Dog) GetType() string {
    return "Dog"
}

func main() {
    var animal AnimalIF
    animal = &Cat{"Yellow"}
    animal.Sleep()  // cat is sleeping
    animal = &Dog{"Black"}
    animal.Sleep()  // dog is sleeping
}
