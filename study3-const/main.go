package main

import "fmt"

// 常量：只读属性
// 常量的声明和var变量声明很像，只需要将关键字修改为const即可

// 还可以通过const来定义枚举类型
/*
const (
    BEIJING = 0
    SHNAGHAI = 1
    SHENZHEN = 2
)
*/
// 通过iota关键字实现与上面枚举一样的赋值功能；
// iota只能够出现在 const() 代码块中
const (
    BEIJING = iota  // 默认iota = 0，之后每一行的值 +1
    SHNAGHAI        // = 1，如果每行想增加10，那么第一行，就可以写成 10*iota
    SHENZHEN        // = 2
)
// iota的高级用法（第一行的公式，可以决定 “初始值” 和 “步长”）
const (
    a, b = 5*iota + 1, 10*iota + 1  //iota = 0，所以a = 1, b = 1
    aa, bb                          //iota = 1，所以aa = 6, bb = 11
    aaa, bbb                        //iota = 2，所以aaa = 11, bbb = 21
    // 下面即使改变公式，iota的值还是继续+1，但是注意，公式变了
    aaaa, bbbb = 2*iota + 1, 3* iota + 2  //iota = 3，所以aaaa = 7, bbbb = 11
    aaaaa, bbbbb                          //iota = 4，所以aaaaa = 9, bbbbb = 14
)

func main() {
    const length int = 10
    fmt.Println("length =", length)

    fmt.Println("a = ", a, ", b = ", b)
    fmt.Println("aa = ", aa, ", bb = ", bb)
    fmt.Println("aaa = ", aaa, ", bbb = ", bbb)
    fmt.Println("aaaa = ", aaaa, ", bbbb = ", bbbb)
    fmt.Println("aaaaa = ", aaaaa, ", bbbbb = ", bbbbb)
}
