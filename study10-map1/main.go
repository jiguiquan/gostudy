package main

import "fmt"

// map的3种声明方式
func main() {
    // 方法1：key类型为string，value类型为string，此时的map为一个空map(没有元素)，不可以直接使用
    var myMap1 map[string]string
    if myMap1 == nil {
        fmt.Println("myMap1为一个空map")
    }
    // 此时的空map是无法使用的，我们需要使用map为其开辟一块空间
    myMap1 = make(map[string]string, 10)

    myMap1["a"] = "aaa"
    myMap1["b"] = "bbb"
    fmt.Println(myMap1)  // map[a:aaa b:bbb]

    // 方法2：直接使用 := make 推断
    myMap2 := make(map[int]string)  // 也可以指定长度，默认为1，但是使用过程中会扩容
    myMap2[1] = "aaa"
    myMap2[2] = "bbb"
    fmt.Println(myMap2)  // map[1:aaa 2:bbb]

    // 方法3：同方法2，但是初始化一批元素
    myMap3 := map[string]string{
        "aa":"aaa",
        "bb":"bbb",
        "cc":"ccc",
    }
    fmt.Println(myMap3) // map[aa:aaa bb:bbb cc:ccc]
}
