package main

import "fmt"

// struct结构体的声明和基本使用
// type：声明一种新的数据类型
type myint int

// 相当于java中的类
type Book struct {
    title string
    auth string
}

func main() {
    var number myint = 10
    fmt.Printf("number = %d, type = %T\n", number, number)  // number = 10, type = main.myint

    var book1 Book
    book1.title = "golang"
    book1.auth = "zhangsan"
    fmt.Printf("%v\n", book1)   // {golang zhangsan}

    // 传值给changeBook()方法，尝试修改内容
    changeBook1(book1)
    fmt.Printf("%v\n", book1)  // {golang zhangsan}，没有修改成功，说明传递的是一个副本值

    changeBook2(&book1)
    fmt.Printf("%v\n", book1)  // {golang lisi}, 只有明确传递指针，才可以在方法中被修改
}

// 副本值传递
func changeBook1(book Book)  {
    book.auth = "lisi"
}

// 指针传递
func changeBook2(book *Book)  {
    book.auth = "wangwu"
}