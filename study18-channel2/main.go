package main

import (
	"fmt"
	"time"
)

/**
  len(c) = 0 , cap(c) = 3
  go程正在运行，现在的长度为： 1 存入的数据为： 0
  go程正在运行，现在的长度为： 2 存入的数据为： 1
  go程正在运行，现在的长度为： 3 存入的数据为： 2
  取到的num= 0
  取到的num= 1
  取到的num= 2
  取到的num= 3
  go程正在运行，现在的长度为： 3 存入的数据为： 3
  main go程结束
  子go程结束！
*/
/**
  结论：
      当channel已经满了后，再向其中插入数据，将会阻塞;
      当channel为空时，从中取元素，也会阻塞
*/
func main() {
	c := make(chan int, 3)                                // 创建一个带有缓存的channel
	fmt.Println("len(c) =", len(c), ", cap(c) =", cap(c)) // len(c) = 0 , cap(c) = 3，有点像 array和 slice，有len和cap

	go func() {
		defer fmt.Println("子go程结束！")

		for i := 0; i < 4; i++ {
			c <- i
			fmt.Println("go程正在运行，现在的长度为：", len(c), "存入的数据为：", i)
		}
	}()

	time.Sleep(1 * time.Second)

	for i := 0; i < 4; i++ {
		num := <-c
		fmt.Println("取到的num=", num)
	}
	fmt.Println("main go程结束")
}
