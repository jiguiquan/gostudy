package main

import (
	"fmt"
	"reflect"
)

// tag：为属性字段作说明
type Resume struct {
	Name string `info:"name" doc:"姓名"`
	Sex  string `info:"sex"`
}

// 通过反射解析出结构体中的tag
func findTag(arg interface{}) {
	t := reflect.TypeOf(arg)
	for i := 0; i < t.NumField(); i++ {
		infoTag := t.Field(i).Tag.Get("info")
		docTag := t.Field(i).Tag.Get("doc")
		fmt.Printf("info = %s, doc=%s\n", infoTag, docTag)
	}
}

func main() {
	resume := Resume{}
	findTag(resume)
	//info = name, doc=姓名
	//info = sex, doc=
}
